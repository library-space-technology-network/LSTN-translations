# Library Space Technology Network (LSTN)

The LSTN project offers public library communities the chance to build and engage with space technology. The [Wolbach Library](https://library.cfa.harvard.edu/) has been working in collaboration with the [Libre Space Foundation](https://libre.space/) on LSTN since summer 2019, and is currently engaged in a pilot to build [SatNOGS](https://satnogs.org/) ground stations at five public libraries around the world. During the pilot, a metadata vocabulary and schema for small satellite missions called [Metasat](https://schema.space/), will be created and tested on the ground stations. The feedback from LSTN participants will ultimately help inform plans to expand the network and build resources to support participating library communities.  

To find out more, visit us at [https://lstn.wolba.ch/](https://lstn.wolba.ch/).

## LSTN translations

<a href="https://hosted.weblate.org/engage/library-space-technology-network/">
<img src="https://hosted.weblate.org/widgets/library-space-technology-network/-/svg-badge.svg" alt="Translation status" />
</a>

We are currently working to translate our [LSTN handbook](https://zenodo.org/record/3776518#.YDAFrXlOk2w) into Spanish so it can be easily accessed by one of our pilot partner libraries, [Biblioteca de Santiago](https://www.bibliotecasantiago.cl/). We are crowdsourcing translations through [Weblate](https://weblate.org/en/); [visit our Weblate page](https://hosted.weblate.org/projects/library-space-technology-network/) to contribute!  

In the future, we hope to translate the handbook into more languages, as well as translate the entire [LSTN website](https://lstn.wolba.ch/).
